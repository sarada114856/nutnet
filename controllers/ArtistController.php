<?php

namespace app\controllers;

use app\helpers\Api;
use app\models\Artist;
use app\models\search\ArtistSearch;
use app\models\UploadForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use function PHPUnit\Framework\throwException;

/**
 * ArtistController implements the CRUD actions for Artist model.
 */
class ArtistController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['index', 'view'],
                            'roles' => ['?','@'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['create', 'update', 'delete', 'get-lastfm-artist-list'],
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ],

        );
    }

    /**
     * Lists all Artist models.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new ArtistSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Artist model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Artist model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function actionCreate(): Response|string
    {
        $model = new Artist();
        $uploadForm = new UploadForm();
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                $uploadForm->save(Artist::tableName(),$model->id);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'uploadForm' => $uploadForm,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Artist model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id): Response|string
    {
        $model = $this->findModel($id);
        $uploadForm = new UploadForm();
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            $uploadForm->save(Artist::tableName(),$model->id,$model->image);
            //echo"<pre>";print_r($result);die();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'uploadForm' => $uploadForm,
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Artist model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete(int $id): Response
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetLastfmArtistList(): bool|string
    {
        if(\Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $result = Api::getRequest([
                'method' => 'artist.search',
                'artist' => $data['name'],
                'api_key' => Yii::$app->params['lastfm_key'],
                'format' => 'json'
            ],'http://ws.audioscrobbler.com/2.0/?');
            return json_encode($result);
        }
        return false;
    }

    /**
     * Finds the Artist model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Artist the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): Artist
    {
        if (($model = Artist::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
