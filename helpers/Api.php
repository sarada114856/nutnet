<?php

namespace app\helpers;

class Api
{

    public static function getRequest(array $data,string $url)
    {
        $final_url =  $url.http_build_query($data);
        $options = array('http' => array(
            'method'  => 'GET',
            'header' => "Content-Type: application/json\r\n",
        ));
        $context  = stream_context_create($options);
        $response = file_get_contents($final_url, false, $context);
        return json_decode($response);
    }
}