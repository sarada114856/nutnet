<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%album}}`.
 */
class m230123_161507_create_album_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%album}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()->comment("Название"),
            'image_url' => $this->string()->null()->comment("Ссылка на изображение"),
            'description' => $this->text()->null()->comment("Описание"),
            'artist_id' => $this->integer()->notNull()->comment("Id исполнителя"),
            'updated_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);


        $this->addForeignKey(
            'fk-album-artist_id',
            'album',
            'artist_id',
            'artist',
            'id',
            'CASCADE',
            'NO ACTION'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-album-artist_id',
            'album'
        );

        $this->dropTable('{{%album}}');
    }
}
