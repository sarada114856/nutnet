<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%upload}}`.
 */
class m230123_163010_create_upload_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%upload}}', [
            'id' => $this->primaryKey(),
            'model' => $this->string('100')->notNull()->comment("Наименование модели для файла"),
            'model_id' => $this->integer()->notNull()->comment("Id модели для файла"),
            'origin' => $this->string('255')->notNull()->comment("Оригинальное название файла"),
            'name' => $this->string('50')->notNull()->comment("Сгенерированное название файла"),
            'ext' => $this->string('20')->notNull()->comment("Формат файла"),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%upload}}');
    }
}
