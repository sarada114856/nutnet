<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%log}}`.
 */
class m230124_130203_create_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger()->defaultValue(1)->notNull()->comment("Тип операции для логирования"),
            'text' => $this->text(),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%log}}');
    }
}
