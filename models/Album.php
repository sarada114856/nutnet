<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "album".
 *
 * @property int $id
 * @property string $title Название
 * @property string|null $description Описание
 * @property string|null $image_url Ссылка на изображение
 * @property int $artist_id Id исполнителя
 * @property int $updated_at
 * @property int $created_at
 *
 * @property Artist $artist
 */
class Album extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'album';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'artist_id'], 'required'],
            [['description'], 'string'],
            [['artist_id', 'updated_at', 'created_at'], 'integer'],
            [['title', 'image_url'], 'string', 'max' => 255],
            [['artist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Artist::class, 'targetAttribute' => ['artist_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
            'image_url' => 'Ссылка на изображение',
            'artist_id' => 'Исполнитель',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function init()
    {
        $this->on(self::EVENT_AFTER_DELETE, function (){
            if($this->image){
                $this->image->deleteFile();
            }
        });
        $this->on(self::EVENT_AFTER_UPDATE,function (){
            $user_id = Yii::$app->user->id;
            Log::createMessage("Пользователь №{$user_id} изменил данные: {$this->asJson()} ",Log::UPDATE_DB_DATA_MESSAGE);
        });
    }

    /**
     * Gets query for [[Artist]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArtist(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Artist::class, ['id' => 'artist_id']);
    }

    public function getImage(): ActiveQuery
    {
        return $this->hasOne(Upload::class, ['model_id' => 'id'])->where(['model' => self::tableName()]);
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'artist_id' => $this->artist_id,
            'image_url' => $this->image_url
        ];
    }

    public function asJson(): bool|string
    {
        return json_encode($this->asArray());
    }
}
