<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "artist".
 *
 * @property int $id
 * @property string $name Имя исполнителя
 * @property string|null $image_url Ссылка на изображение
 * @property int $updated_at
 * @property int $created_at
 *
 * @property Album[] $albums
 */
class Artist extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'artist';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['updated_at', 'created_at'], 'integer'],
            [['name','image_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Имя исполнителя',
            'image_url' => 'Ссылка на изображение',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function init()
    {
        $this->on(self::EVENT_AFTER_DELETE, function (){
            if($this->image){
                $this->image->deleteFile();
            }
        });
        $this->on(self::EVENT_AFTER_UPDATE,function (){
            $user_id = Yii::$app->user->id;
            Log::createMessage("Пользователь №{$user_id} изменил данные: {$this->asJson()} ",Log::UPDATE_DB_DATA_MESSAGE);
        });
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * Gets query for [[Albums]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlbums(): ActiveQuery
    {
        return $this->hasMany(Album::class, ['artist_id' => 'id']);
    }

    public function getImage(): ActiveQuery
    {
        return $this->hasOne(Upload::class, ['model_id' => 'id'])->where(['model' => self::tableName()]);
    }

    public static function getArtistList(): array
    {
       return  ArrayHelper::map(self::find()->all(),'id', 'name');
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image_url' => $this->image_url
        ];
    }

    public function asJson(): bool|string
    {
        return json_encode($this->asArray());
    }
}
