<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property int $type Тип операции для логирования
 * @property string|null $text
 * @property int $created_at
 */
class Log extends \yii\db\ActiveRecord
{

    public const UPDATE_DB_DATA_MESSAGE = 1;

    public static array $typeRange = [
        self::UPDATE_DB_DATA_MESSAGE => "Изменение данных пользователем"
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['type', 'created_at'], 'integer'],
            [['text'], 'string'],
        ];
    }

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => [],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'text' => 'Text',
            'created_at' => 'Created At',
        ];
    }

    public static function createMessage(string $message, int $type){
        $log = new self();
        $log->text = $message;
        $log->type = $type;
        $log->save();
    }
}
