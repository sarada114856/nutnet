<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "upload".
 *
 * @property int $id
 * @property string $model Наименование модели для файла
 * @property int $model_id Id модели для файла
 * @property string $origin Оригинальное название файла
 * @property string $name Сгенерированное название файла
 * @property string $ext Формат файла
 * @property int $created_at
 */
class Upload extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'upload';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['model', 'model_id', 'origin', 'name', 'ext'], 'required'],
            [['model_id', 'created_at'], 'integer'],
            [['model'], 'string', 'max' => 100],
            [['origin'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 50],
            [['ext'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'model' => 'Model',
            'model_id' => 'Model ID',
            'origin' => 'Origin',
            'name' => 'Name',
            'ext' => 'Ext',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => [],
                ],
            ],
        ];
    }

    public static function saveFile(string $model, int $model_id, array $file): ?Upload
    {
        $upload = new Upload();
        $upload->model = $model;
        $upload->model_id = $model_id;
        $upload->origin = $file['origin'];
        $upload->name = $file['name'];
        $upload->ext = $file['ext'];

        return $upload->save() ? $upload : null;
    }

    public function deleteFile(): bool|int
    {
        if(file_exists($this->getLink(false)))
        {
            unlink($this->getLink(false));
        }
        if(file_exists($this->getThumbnail(false)))
        {
            unlink($this->getThumbnail(false));
        }
        return $this->delete();
    }

    public function getRealName(): string
    {
        return $this->origin . '.' . $this->ext;
    }

    public function getLink(bool $absolute = true): string
    {
        return ($absolute ? '/' : '') . $this->tableName() .'/' . $this->model . '/' . $this->name . '.' . $this->ext;
    }

    public function getThumbnail(bool $absolute = true): string
    {
        if(file_exists($this->tableName() .'/thumbnail/' . $this->name . '.' . $this->ext))
            return ($absolute ? '/' : '') . $this->tableName() .'/thumbnail/' . $this->name . '.' . $this->ext;
        else
            return $this->getLink($absolute);
    }

    public static function checkExist(string $filename,string $model = null): bool
    {
        $uploadPath = Yii::getAlias('@app') . '/web/upload/';
        $thumbnailPath = Yii::getAlias('@app') . '/web/upload/thumbnail/';
        if($model)
        {
            $uploadPath = $uploadPath . $model . '/';
            if(!is_dir($uploadPath)) mkdir($uploadPath);
            if(!is_dir($thumbnailPath)) mkdir($thumbnailPath);
        }
        return file_exists($uploadPath . $filename);
    }
}
