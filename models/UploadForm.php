<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{

    public array $files = [];
    public array $fileNames = [];

    public string $uploadPath = 'upload/';
    public string $thumbnailPath = 'upload/thumbnail/';

    public function rules(): array
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => false, 'on' => 'hasNotSkip']
        ];
    }

    public function getFileNames(): array
    {
        return $this->fileNames;
    }

    public function upload(string $model = null): bool
    {
        if ($this->validate()) {
            foreach ($this->files as $file) {
                do{
                    $filename = Yii::$app->getSecurity()->generateRandomString(15);
                }while ($this->checkExist($filename . '.' . $file->extension, $model));
                $file->saveAs($this->uploadPath . ($model ? $model . '/' : '') . $filename . '.' . $file->extension);
                if(in_array($file->extension,['jpg','png','jpeg', 'heif']))
                {
                    $path = $this->uploadPath . ($model ? $model . '/' : '') . $filename . '.' . $file->extension;
                    $image = Yii::getAlias($path);
                }
                $this->addFileNames($filename,$file->baseName,$file->extension);
            }
            return true;
        } else {
            return false;
        }
    }

    public function save(string $model, int $modelId, Upload $deleteModel = null): array
    {
        $files = [];
        $this->files = UploadedFile::getInstances($this, 'files');
        if(count($this->files))
        {
            if($deleteModel) $deleteModel->deleteFile();
            if ($this->upload($model)) {
                if($this->getFileNames()){
                    foreach ($this->getFileNames() as $file)
                    {
                        if($upload = Upload::saveFile($model,$modelId,$file))
                        {
                            $files[] = $upload;
                        }
                    }
                }
            }else{
                if($this->errors)
                    throw new \Exception($this->errors['files'][0]);
            }
        }

        return $files;
    }

    public function checkExist(string $filename, string $model = null): bool
    {
        $dir = $this->uploadPath;
        if($model)
        {
            $dir = $this->uploadPath . $model . '/';
            if(!is_dir($dir)) mkdir($dir);
            if(!is_dir($this->thumbnailPath)) mkdir($this->thumbnailPath);
        }
        return file_exists($dir . $filename);
    }

    public function addFileNames(string $name, string $origin, string $ext)
    {
        $files = $this->fileNames;
        $files[] = [
            'name' => $name,
            'origin' => $origin,
            'ext' => $ext,
        ];
        $this->fileNames = $files;
    }
}