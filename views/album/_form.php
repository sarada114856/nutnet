<?php

use app\models\Artist;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Album $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\UploadForm $uploadForm */
?>

<div class="album-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_url')->hiddenInput(['maxlength' => true])->label('') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'artist_id')->dropDownList(Artist::getArtistList()) ?>

    <?= $form->field($uploadForm, 'files[]')->fileInput(['class' => 'mt-3']) ?>

    <?php if($model->image):?>
        <div class="mt-3">
            <label>Фото альбома: </label>
            <a href="<?=$model->image->getLink()?>" target="_blank">
                <img src="<?=$model->image->getLink()?>" width="150px" class="rounded_image" >
            </a>
        </div>
    <?php endif;?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success mt-3']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
