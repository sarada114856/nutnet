<?php

use app\models\Album;
use app\models\Artist;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\search\AlbumSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Альбомы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить альбом', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            'description:ntext',
            [
                'attribute' => 'artist_id',
                'filter' => Artist::getArtistList(),
                'format' => 'raw',
                'filterInputOptions' => ['prompt' => 'Все исполнители', 'class' => 'form-control'],

                'value' => function($model)
                {
                    {
                        $artist = $model->artist;
                        if($artist)
                            return $artist->name;
                        else
                            return "Кажется пользователь куда-то пропал...";
                    }
                }
            ],
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($model)
                {
                    if($model->image){
                        return "<a href='{$model->image->getLink()}' target='_blank'>
                                    <img src='{$model->image->getLink()}' width='150px' class='rounded_image'>
                                </a>";
                    }else{
                        return "<p>Изображение отсутствует</p>";
                    }
                }
            ],
            [
                'label' => 'Изображение c lastfm',
                'format' => 'raw',
                'value' => function($model)
                {
                    if($model->image_url){
                        return "<a href='{$model->image_url}' target='_blank'>
                                    <img src='{$model->image_url}' width='150px' class='rounded_image'>
                                </a>";
                    }else{
                        return "<p>Изображение отсутствует</p>";
                    }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Album $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
