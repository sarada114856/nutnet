<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Album $model */
/** @var app\models\UploadForm $uploadForm */

$this->title = 'Редактировать альбом: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Альбомы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="album-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'uploadForm' => $uploadForm,
    ]) ?>

</div>
