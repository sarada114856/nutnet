<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Album $model */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Альбомы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="album-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить альбом?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title',
            'description:ntext',
            [
                'attribute' => 'artist_id',
                'value' => function($model)
                {
                    $artist = $model->artist;
                    if($artist)
                        return $artist->name;
                    else
                        return "Кажется пользователь куда-то пропал...";
                }
            ],
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($model)
                {
                    if($model->image){
                        return "<a href='{$model->image->getLink()}' target='_blank'>
                                    <img src='{$model->image->getLink()}' width='150px' class='rounded_image'>
                                </a>";
                    }else{
                        return "<p>Изображение отсутствует</p>";
                    }
                }
            ],
            [
                'label' => 'Изображение c lastfm',
                'format' => 'raw',
                'value' => function($model)
                {
                    if($model->image_url){
                        return "<a href='{$model->image_url}' target='_blank'>
                                    <img src='{$model->image_url}' width='150px' class='rounded_image'>
                                </a>";
                    }else{
                        return "<p>Изображение отсутствует</p>";
                    }
                }
            ],
        ],
    ]) ?>

</div>
