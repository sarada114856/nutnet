<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Artist $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\UploadForm $uploadForm */


?>

<div class="artist-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_url')->hiddenInput(['maxlength' => true])->label('') ?>

    <?= $form->field($uploadForm, 'files[]')->fileInput(['class' => 'mt-3']) ?>

    <?php if($model->image):?>
        <div class="mt-3">
            <label>Фото исполнителя: </label>
             <a href="<?=$model->image->getLink()?>" target="_blank"><img src="<?=$model->image->getLink()?>" width="200px" style="padding-bottom: 40px" > </a>
        </div>
    <?php endif;?>

    <div class="form-group mt-3">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
