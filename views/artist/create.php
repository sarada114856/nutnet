<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Artist $model */
/** @var app\models\UploadForm $uploadForm */

$this->title = 'Добавить исполнителя';
$this->params['breadcrumbs'][] = ['label' => 'Исполнители', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artist-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'uploadForm' => $uploadForm,
    ]) ?>

</div>
