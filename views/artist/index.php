<?php

use app\models\Artist;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\search\ArtistSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Исполнители';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artist-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить исполнителя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($model)
                {
                    if($model->image){
                        return "<a href='{$model->image->getLink()}' target='_blank'>
                                    <img src='{$model->image->getLink()}' width='150px' class='rounded_image'>
                                </a>";
                    }else{
                        return "<p>Изображение отсутствует</p>";
                    }
                }
            ],
            [
                'label' => 'Изображение c lastfm',
                'format' => 'raw',
                'value' => function($model)
                {
                    if($model->image_url){
                        return "<a href='{$model->image_url}' target='_blank'>
                                    <img src='{$model->image_url}' width='150px' class='rounded_image'>
                                </a>";
                    }else{
                        return "<p>Изображение отсутствует</p>";
                    }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Artist $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
