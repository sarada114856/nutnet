$( function() {
    $( "#artist-name" ).autocomplete({
        source: function( request, response ) {
            const _csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '/artist/get-lastfm-artist-list',
                type: 'POST',
                data: {_csrf, name:request.term},
                success: function(res){
                    let data = JSON.parse(res);
                    console.log(data)
                    if(data.results.artistmatches){
                        let list = [];
                        (data.results.artistmatches.artist).map((value) => {
                            let tmp = value.image[2];
                            list.push({value: value.name, image_url: Object.values(tmp)[0]})
                        })

                        response( list );
                    }
                },
                error: function(res){
                    console.log(res)
                }
            });
        },
        select: function( event, ui ) {
            console.log([event, ui.item]);
            if(ui.item.image_url){
                document.getElementById("artist-image_url").value = ui.item.image_url;
            }
        },
        minLength: 3,
    });
} );

$( function() {
    $( "#album-title" ).autocomplete({
        source: function( request, response ) {
            const _csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '/album/get-lastfm-album-list',
                type: 'POST',
                data: {_csrf, name:request.term},
                success: function(res){
                    let data = JSON.parse(res);
                    console.log(data);
                    if(data.results.albummatches){
                        let list = [];
                        (data.results.albummatches.album).map((value) => {
                            let tmp = value.image[2];
                            list.push({value: value.name, image_url: Object.values(tmp)[0]})
                        })
                        response( list );
                    }
                },
                error: function(res){
                    console.log(res)
                }
            });
        },
        select: function( event, ui ) {
            console.log([event, ui.item]);
            if(ui.item.image_url){
                document.getElementById("album-image_url").value = ui.item.image_url;
            }
        },
        minLength: 3,
    });
} );